package com.azureorange.picohan;

import android.app.DirectAction;
import android.util.Log;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {

    public String title;
    public String question;
    List<String> playerArray;


    public Game(String tempQuestion) {
        super();

        // set the title
        title = "Game";

        // get Players and Player count
        playerArray = new LinkedList<String>(Arrays.asList(PlayerActivity.getPlayerList()));
        int playerCount = PlayerActivity.getPlayerCount();

        Pattern primaryPattern = Pattern.compile("\\[PRIMARY\\]");
        Matcher primaryMatcher = primaryPattern.matcher(tempQuestion);

        if (primaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            tempQuestion = tempQuestion.replace(primaryMatcher.group(), playerArray.get(index));
            playerArray.remove(index);
            playerCount = playerCount - 1;
        }

        Pattern secondaryPattern = Pattern.compile("\\[SECONDARY\\]");
        Matcher secondaryMatcher = secondaryPattern.matcher(tempQuestion);

        if (secondaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            Log.d("Fuck 3", "index:" + index + ", match:" + secondaryMatcher.group() + ", player:" + playerArray.get(index));
            tempQuestion = tempQuestion.replace(secondaryMatcher.group(), playerArray.get(index));
            playerArray.remove(index);
        }

        question = tempQuestion;
    }

    private DirectAction getIntent() {
        return null;
    }

    public String getGame() {
        return question;
    }

    public String getTitle() {
        return title;
    }
}
