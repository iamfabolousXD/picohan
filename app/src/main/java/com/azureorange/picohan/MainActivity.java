package com.azureorange.picohan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Display drinking disclaimer message
        popupMessage("Disclaimer", "You should drink responsibly!");

    }



    public void popupMessage(String msgTitle, String msgBody){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msgBody);
        alertDialogBuilder.setTitle(msgTitle);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("ok", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("drinking_disclaimer","Ok btn pressed");
                // Switch to PlayerActivity
                switchActivities();
            }

        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Method to switch Activity
    private void switchActivities() {
        Intent switchActivityIntent = new Intent(this, PlayerActivity.class);
        startActivity(switchActivityIntent);
    }

}