package com.azureorange.picohan;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class PicohanActivity extends AppCompatActivity {

    int count = 1;
    String answer = "";
    public int virusCount = 0;
    List<String> questionArray;
    List<String> virusArray;
    List<String> gameArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If the Android version is lower than Jellybean, use this call to hide
        // the status bar. https://developer.android.com/training/system-ui/status
//        if (Build.VERSION.SDK_INT > 16) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }

        setContentView(R.layout.activity_picohan);

        // lock orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Set game mode and pick the corresponding question array
        String gameMode = "default";
        final int maxTurns = 30;

        ArrayAdapter<String> questionAdapter;
        ArrayAdapter<String> virusAdapter;
        ArrayAdapter<String> gameAdapter;

        if (gameMode.equals("default")) {
            questionArray = new LinkedList<String>(Arrays.asList(getResources().getStringArray(R.array.default_questions)));
            virusArray = new LinkedList<String>(Arrays.asList(getResources().getStringArray(R.array.viruses)));
            gameArray = new LinkedList<String>(Arrays.asList(getResources().getStringArray(R.array.games)));
        }

        questionAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                questionArray
        );

        setListAdapter(questionAdapter);

        virusAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                questionArray
        );

        setListAdapter(virusAdapter);

        gameAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                gameArray
        );

        setListAdapter(gameAdapter);

        RelativeLayout touch = (RelativeLayout) findViewById(R.id.touch);
        final TextView questionText = (TextView) findViewById(R.id.question);
        final TextView questionTitle = (TextView) findViewById(R.id.title);

        touch.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {

                String question;
                String displayedText;
                String displayedTitle;

                if (count <= maxTurns) {

                    // check if virus need to be cured
                    if ( virusCount > 0 && count % 5 == 0) {
                        displayedText = new Virus(answer).getVirus();
                        displayedTitle = "Cure";
                        virusCount--;
                    } else {
                        // Deploy cards

                        // every 5th turn triggers a special mode
                        if (count % 5 == 0) {
                            // 50:50 if it is a game or a virus
                            Random rand = new Random();
                            int i = rand.nextInt(1001);
                            i++;
                            if (i % 2 == 0 && count <= (maxTurns - 4) && virusArray.size() > 1) {
                                // virus triggered
                                // TODO: end Virus after random interval. (and game over stops all eventually open viruses)
                                // pick random index
                                int index = new Random().nextInt(virusArray.size());
                                if (index % 2 != 0) {
                                    index--;
                                }
                                question = setVirus(index);
                                answer = getCure(index);
                                displayedText = new Virus(question).getVirus();
                                displayedTitle = "Virus";
                                virusCount++;
                            } else {
                                // game triggered
                                question = setGame();
                                displayedText = new Game(question).getGame();
                                displayedTitle = "Game";
                            }
                        } else {
                            // normal question triggered
                            question = setQuestion();
                            displayedText = new Question(question).getQuestion();
                            displayedTitle = new Question(question).getTitle();
                        }
                        count = count + 1;
                    }

                    questionText.setText(displayedText);
                    questionTitle.setText(displayedTitle);
                    setActivityBackgroundColor(retrieveColor(displayedTitle));

                } else{
                    if (count == (maxTurns + 1)) {
                        // set count to zero and display game over message
                        count = 9999;
                        questionText.setText("");
                        questionTitle.setText("Game Over");
                        setActivityBackgroundColor(retrieveColor("Game Over"));
                    } else {
                        // The game is over now
                        switchActivities();
                    }
                }
            }
        });

    }

    private String setQuestion() {
        // pick random index
        int index = new Random().nextInt(questionArray.size());
        // set q to be the string from the array
        String q = questionArray.get(index);
        // remove the question
        questionArray.remove(index);

        return q;
    }

    private String setVirus(int index) {
        // set q to be the string from the array
        String q = virusArray.get(index);
        // remove the question
        virusArray.remove(index);

        return q;
    }

    private String getCure(int index) {
        // set q to be the string from the array
        String q = virusArray.get(index);
        // remove the question
        virusArray.remove(index);

        return q;
    }

    private String setGame() {
        // pick random index
        int index = new Random().nextInt(gameArray.size());
        // set q to be the string from the array
        String q = gameArray.get(index);
        // remove the question
        gameArray.remove(index);

        return q;
    }

    public String retrieveColor(String title) {

        String hexColor;
        int intColor;

        if (Objects.equals(title, "Virus")) {
            hexColor = String.valueOf(R.color.virus_card);
            hexColor = "#E17D4F";
            intColor = R.color.virus_card;
        } else if (Objects.equals(title, "Cure")) {
            hexColor = String.valueOf(R.color.cure_card);
            hexColor = "#643A71";
            intColor = R.color.cure_card;
        } else if (Objects.equals(title, "Game")) {
            hexColor = String.valueOf(R.color.game_card);
            hexColor = "#66B55D";
            intColor = R.color.game_card;
        } else if (Objects.equals(title, "Bottoms up")) {
            hexColor = String.valueOf(R.color.bottoms_up_card);
            hexColor = "#DD5050";
            intColor = R.color.bottoms_up_card;
        } else if (Objects.equals(title, "Game Over")) {
            hexColor = String.valueOf(R.color.game_over_card);
            hexColor = "#392B3D";
            intColor = R.color.game_over_card;
        } else {
            hexColor = String.valueOf(R.color.default_card);
            hexColor = "#8B5FBF";
            intColor = R.color.default_card;
        }

        Log.d("Color", "returned Title: " + title);
        Log.d("Color", "returned Color: " + hexColor);

        return hexColor;
    }

    public void setActivityBackgroundColor(String hexColor) {
//        int color = Color.parseColor(hexColor);

        RelativeLayout main = (RelativeLayout) findViewById(R.id.touch);
        main.setBackgroundColor(Color.parseColor(hexColor));
    }

    private void setListAdapter(ArrayAdapter<String> adapter) {
    }

    // Method to switch Activity
    private void switchActivities() {
        Intent switchActivityIntent = new Intent(this, PlayerActivity.class);
        startActivity(switchActivityIntent);
    }

}
