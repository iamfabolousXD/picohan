package com.azureorange.picohan;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class PlayerActivity extends AppCompatActivity {

    public static String[] playerList;
    public static int playerCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If the Android version is lower than Jellybean, use this call to hide
        // the status bar. https://developer.android.com/training/system-ui/status
//        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }

        setContentView(R.layout.activity_player);

        //TODO: Dialogfeld kann umgangen werden und die App bleibt stehn.

        // lock orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Define TextInputEditText from activity_player.xml
        TextInputEditText player_input_field = findViewById(R.id.player_name_input_field);
        // Define FloatingActionButton from activity_player.xml
        Button addPlayerButton = findViewById(R.id.add_player_button);
        // Define TextViews from activity_player.xml
        Button Player01 = findViewById(R.id.nameTagButton01);
        Button Player02 = findViewById(R.id.nameTagButton02);
        Button Player03 = findViewById(R.id.nameTagButton03);
        Button Player04 = findViewById(R.id.nameTagButton04);
        Button Player05 = findViewById(R.id.nameTagButton05);
        Button Player06 = findViewById(R.id.nameTagButton06);
        Button Player07 = findViewById(R.id.nameTagButton07);
        Button Player08 = findViewById(R.id.nameTagButton08);

        // Define a TextView array to cycle through
        Button[] playerNameButton = {
                Player01, Player02, Player03, Player04, Player05, Player06, Player07, Player08
        };
        // Get max player number from TextView count
        int max_players = 8;
        int min_players = 2;

        // Define player_list as an array of Strings
        final String[] player_list = new String[max_players];

        // Counter to iterate through player count while clicking
        final int[] count = {0};

        // What happens when the button is clicked?
        addPlayerButton.setOnClickListener(
            view -> {

                if (count[0] < max_players) { // make sure player limit is not exceeded
                    // Add input to player_list
                    player_list[count[0]] = Objects.requireNonNull(player_input_field.getText()).toString();

                    // set the TextView to display the player name
                    playerNameButton[count[0]].setText(player_list[count[0]]);
                    playerNameButton[count[0]].setVisibility(View.VISIBLE);

                    // clear the inputField
                    player_input_field.getText().clear();

                    // count + 1
                    count[0] = count[0] + 1;
                    playerCount = count[0];
                } else {
                    // if max player limit reached, simply do nothing
                    player_input_field.getText().clear();
                }
            }
        );

        // starting the game

        Button startButton = findViewById(R.id.start_button);

        playerList = player_list;

        startButton.setOnClickListener(
                view -> {
                    int length = count[0];
                    if (length < min_players) {
                        popupMessage("Player Count", "You need at least two players to play this game.");
                    } else {
                        switchActivities();
                    }
                }
        );

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        View decorView = getWindow().getDecorView();
//        // Hide the status bar.
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//        // Remember that you should never show the action bar if the
//        // status bar is hidden, so hide that too if necessary.
//        ActionBar actionBar = getActionBar();
//        actionBar.hide();
//    }

    public void popupMessage(String msgTitle, String msgBody){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msgBody);
//        alertDialogBuilder.setIcon(R.drawable.ic_no_internet);
        alertDialogBuilder.setTitle(msgTitle);
        alertDialogBuilder.setNegativeButton("ok", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("player_count","Ok btn pressed");
                // add these two lines, if you wish to close the app:
//                finishAffinity();
//                System.exit(0);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static String[] getPlayerList() {
        return playerList;
    }

    public static int getPlayerCount() {
        Log.d("Fuck", "playerCount:" + playerCount);
        return playerCount;
    }

    // Method to switch Activity
    private void switchActivities() {
        Intent switchActivityIntent = new Intent(this, PicohanActivity.class);
        startActivity(switchActivityIntent);
    }

}