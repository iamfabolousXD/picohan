package com.azureorange.picohan;

import static android.content.Intent.getIntent;

import android.app.DirectAction;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question {

    public String title;
    public String question;
    List<String> playerArray;


    public Question(String tempQuestion) {
        super();

        // search for the title pattern and extract the title
        Pattern titlePattern = Pattern.compile("\\[\\[(.*?)\\]\\]");
        Matcher titleMatcher = titlePattern.matcher(tempQuestion);

        // remove the title from the question and assign the title and question variable
        // if group is 1 the regex is excluded, if it is 0, it is included
        if (titleMatcher.find()) {
//            tempQuestion = srcQuestion.replace("[[" + titleMatcher.group(1).trim() + "]]", "");
            tempQuestion = tempQuestion.replace(titleMatcher.group(0).trim(), "");
            title = titleMatcher.group(1).trim();
        } else {
            title = "No title found";
        }

        // search for the player pattern and extract the title
//        Pattern playerPattern = Pattern.compile("\\[PLAYER[0-9]+\\]");
//        Matcher playerMatcher = playerPattern.matcher(tempQuestion);

        playerArray = new LinkedList<String>(Arrays.asList(PlayerActivity.getPlayerList()));
        int playerCount = PlayerActivity.getPlayerCount();
//        int playerCount = playerArray.size();

        Pattern primaryPattern = Pattern.compile("\\[PRIMARY\\]");
        Matcher primaryMatcher = primaryPattern.matcher(tempQuestion);

        Log.d("Fuck", tempQuestion);
        Log.d("Fuck", "player count:" + playerCount);

        if (primaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            Log.d("Fuck 2", "index:" + index + ", match:" + primaryMatcher.group() + ", player:" + playerArray.get(index));
            tempQuestion = tempQuestion.replace(primaryMatcher.group(), playerArray.get(index));
            playerArray.remove(index);
            playerCount = playerCount - 1;
//            playerCount = playerArray.size();
        }

        Pattern secondaryPattern = Pattern.compile("\\[SECONDARY\\]");
        Matcher secondaryMatcher = secondaryPattern.matcher(tempQuestion);

        if (secondaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            Log.d("Fuck 3", "index:" + index + ", match:" + secondaryMatcher.group() + ", player:" + playerArray.get(index));
            tempQuestion = tempQuestion.replace(secondaryMatcher.group(), playerArray.get(index));
            playerArray.remove(index);
            playerCount = playerCount - 1;
//            playerCount = playerArray.size();
        }

        Pattern teamPattern = Pattern.compile("\\[TEAM\\]");
        Matcher teamMatcher = teamPattern.matcher(tempQuestion);

        int teamCount = 0;
        while (teamMatcher.find()) {
            teamCount++;
            Log.d("Fuck EY!","counter: " + teamCount);
        }

        teamMatcher = teamPattern.matcher(tempQuestion);

        //TODO: pick random teams
        //TODO: assign team names

        if (teamMatcher.find()) {

            Log.d("Fuck this:", "team count:" + teamCount + ", " + tempQuestion);
            ArrayList<String> teamString = new ArrayList<String>();
            for (int j = 0; j < teamCount; j++) {
                teamString.add("Team" + j);
            }
            for (int i = 0; i < playerCount; i += teamCount) {
                for (int j = 0; j < teamCount; j++) {
//                    String tempString = teamString.get(j);
//                    teamString.set(j, playerArray.get(i + j));
                    if (teamString.contains("Team" + j)) {
                        teamString.set(j, playerArray.get(i + j));
                    } else {
                        teamString.set(j, teamString.get(j) + " and " + playerArray.get(i + j));
                    }
                }
            }

            for (int i = 0; i < teamCount; i++) {
                Log.d("Fuck final", "team: " + teamString.get(i));
                tempQuestion = tempQuestion.replaceFirst("\\[TEAM\\]", teamString.get(i));
            }
        }

        question = tempQuestion;
    }

    private DirectAction getIntent() {
        return null;
    }

    public String getQuestion() {
        return question;
    }

    public String getTitle() {
        return title;
    }
}
