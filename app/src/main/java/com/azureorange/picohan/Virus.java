package com.azureorange.picohan;

import android.app.DirectAction;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Virus {

    public String title;
    public String question;
    public static String infectedPlayerName;
    public static String infected2PlayerName;
    List<String> playerArray;


    public Virus(String tempQuestion) {
        super();

        // set the title
        title = "Virus";

        // get Players and Player count
        playerArray = new LinkedList<String>(Arrays.asList(PlayerActivity.getPlayerList()));
        int playerCount = PlayerActivity.getPlayerCount();

        // change [PRIMARY] to player name
        Pattern primaryPattern = Pattern.compile("\\[PRIMARY\\]");
        Matcher primaryMatcher = primaryPattern.matcher(tempQuestion);

        if (primaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            // set persistent infectedPlayerName for the Cure later
            infectedPlayerName = playerArray.get(index);
            tempQuestion = tempQuestion.replace(primaryMatcher.group(), infectedPlayerName);
            Log.d("What the fuck", "virus-primary: " + infectedPlayerName);
            playerArray.remove(index);
            playerCount = playerCount - 1;
        }

        // change [NAME] to player name
        Pattern namePattern = Pattern.compile("\\[NAME\\]");
        Matcher nameMatcher = namePattern.matcher(tempQuestion);

        if (nameMatcher.find()) {
            Log.d("What the fuck", "cure-primary: " + infectedPlayerName);
            tempQuestion = tempQuestion.replace(nameMatcher.group(), infectedPlayerName);
        }

        Pattern secondaryPattern = Pattern.compile("\\[SECONDARY\\]");
        Matcher secondaryMatcher = secondaryPattern.matcher(tempQuestion);

        if (secondaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            infected2PlayerName = playerArray.get(index);
            tempQuestion = tempQuestion.replace(secondaryMatcher.group(), infected2PlayerName);
            playerArray.remove(index);
        }

        // change [NAME] to player name
        Pattern name2Pattern = Pattern.compile("\\[NAME2\\]");
        Matcher name2Matcher = name2Pattern.matcher(tempQuestion);

        if (name2Matcher.find()) {
            tempQuestion = tempQuestion.replace(name2Matcher.group(), infected2PlayerName);
        }

        question = tempQuestion;
    }

    private DirectAction getIntent() {
        return null;
    }

    public String getVirus() {
        return question;
    }

    public String getTitle() {
        return title;
    }
}
